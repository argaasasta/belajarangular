import { PortofolioAppPage } from './app.po';

describe('portofolio-app App', () => {
  let page: PortofolioAppPage;

  beforeEach(() => {
    page = new PortofolioAppPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
